On this step I impleted a method to insert nodes by its bounding box. There is some shapes classes and a collision manager that detects an AABB collision.
Now it's possible to get a list of objects inside a quadtree node based on an bounding box position.

The example utilises the Godots _Draw function and it's heavy on the resources part. 