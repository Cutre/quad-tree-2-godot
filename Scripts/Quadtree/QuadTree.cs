using Godot;
using System;

using MyPhysics.Colliders;
using System.Collections.Generic;

namespace Quadtree{
    /// <summary>
    /// A QuadTree class with a generic to hold the data.
    /// <para>Have two methods of insertion: point based and <see cref="QuadShape"/> based.
    /// </summary>
    /// <typeparam name="T">Data</typeparam>
    public class QuadTree<T>
    {   
        #region Properties
        /// <summary>
        /// The shape of this part of the tree
        /// </summary>
        /// <value></value>
        public QuadShape MyShape{get;set;}

         /// <summary>
        /// Array of nodes
        /// </summary> 
        public QNode<T>?[] MyNodes{get;set;}  
        #endregion               
        
        #region QuadTree<T> childs
        public QuadTree<T> TopLeftTree{get;set;}
        public QuadTree<T> TopRigthTree{get;set;}
        public QuadTree<T> BottomLeftTree{get;set;}
        public QuadTree<T> BottomRigthTree{get;set;}
        #endregion

        #region Extended Properties
        /// <summary>
        /// The number of Quadtree children
        /// </summary>
        public byte Children{
            get{return 4;}
        }

        /// <summary>
        /// Capacity of the node array
        /// </summary>
        public int Capacity{
            get {return 4;}
        }
        #endregion

        #region Constructors
        public QuadTree(){
            this.MyShape = new QuadShape(Vector2.Zero, Vector2.Zero);                   
        }

        public QuadTree(in Vector2 topLeft, in Vector2 bottomRigth){
            this.MyShape = new QuadShape(topLeft, bottomRigth);
            this.MyNodes = new QNode<T>?[this.Capacity];

        }
        #endregion
       
        #region Insert 
        /// <summary>
        /// Inserts a <see cref="QNode<T>"/> with the data in the correct QuadTree
        /// </summary>
        /// <param name="pos">The position of the object</param>
        /// <param name="obj">The data object</param>
        /// <returns>Is the node inserted?</returns>
        public bool Insert(in Vector2 pos, in T obj){            
            if(this.Boundary(pos) == false){                              
                return false;
            }

            int index;

            if(this.TopLeftTree == null){
                if(this.CheckArray(out index)){               
                    //Check if the object is in the array    
                    if(CheckObjInArray(obj) == false){
                        this.MyNodes[index] = new QNode<T>(obj, pos, null);
                        return true; 
                    }     
                    //GD.Print("Object in the array");
                    return false;                              
                }                
                this.Subdivide(this);
            }

            if(this.TopLeftTree.Insert(pos, obj)){                              
                return true;
            }            
            else if(this.TopRigthTree.Insert(pos, obj)){                
                return true;
            }            
            else if(this.BottomRigthTree.Insert(pos, obj)){                
                return true;
            }           
            else if(this.BottomLeftTree.Insert(pos, obj)){                
                return true;
            }            
            return false; 
        }

        public bool Insert( in QuadShape quad, in T obj){
             if(this.Boundary(quad) == false){                              
                return false;
            }

            int index;
            
            if(this.TopLeftTree == null){                
                if(this.CheckArray(out index)){               
                    //Get one of the points that are inside this quad shape
                    Vector2 point = this.CheckPoints(quad);
                    this.MyNodes[index] = new QNode<T>(obj, point, quad);                    
                    return true;                              
                }       
                this.Subdivide(this);
            }

            //OJU! Here there's no checking if the quad is inserted because the points can be at differents quads.
            //That's why we use true but we don't know if the node is inserted or not
            this.TopLeftTree.Insert(quad, obj);                     
            this.TopRigthTree.Insert(quad, obj);                
            this.BottomRigthTree.Insert(quad, obj);                
            this.BottomLeftTree.Insert(quad, obj);
            return true;         
        }
        #endregion

        #region Checkers
        private Vector2 CheckPoints(in QuadShape quad){
            for(int i = 0; i < 4; i++){
                if(this.MyShape.Intersects(quad[i])){
                    return quad[i];
                }
            }
            return new Vector2(-5000, -5000);
        }     

        /// <summary>
        /// Checks if an object is in the array
        /// </summary>
        /// <param name="obj">Object to check</param>
        /// <returns>It is in the array?</returns>
        private bool CheckObjInArray(in T obj){
            for(int i = 0; i < this.MyNodes.Length; i++){
                if(this.MyNodes[i] == null){
                    continue;
                }

                if(obj.Equals(this.MyNodes[i].Value.MyData)){
                    return true;
                }
            }
            return false;
        }
        #endregion

        #region GetObjects
        /// <summary>
        /// Gets the objects saved at the node where the shape collides
        /// </summary>
        /// <param name="shape">Shape to check collision</param>
        /// <returns>List of the objects</returns>
        public List<T> GetObjects(in QuadShape shape){
            List<T> objects = new List<T>();            
            //If not at range, abort :'(
            if(this.Boundary(shape) == false){
                //GD.Print("error 1");
                return objects;
            }            

            if(this.MyNodes[0] != null){                
                for(int i = 0; i < this.MyNodes.Length; i++){
                    if(this.MyNodes[i] == null){
                        return objects;
                    }
                    objects.Add(this.MyNodes[i].Value.MyData);
                }
            }            

            if(this.TopLeftTree == null){
                return objects;
            }

            objects.AddRange(this.TopLeftTree.GetObjects(shape));
            objects.AddRange(this.TopRigthTree.GetObjects(shape));
            objects.AddRange(this.BottomLeftTree.GetObjects(shape));
            objects.AddRange(this.BottomRigthTree.GetObjects(shape));

            return objects;
        }
        #endregion

        #region Boundarys
        /// <summary>
        /// Checks if a point is inside a rectangle.
        /// </summary>
        /// <param name="pos">The point pos</param>
        private bool Boundary(in Vector2 pos){            
            return MyShape.Intersects(pos);
        }

        private bool Boundary(in QuadShape shape){            
            return MyShape.Intersects(shape);
        }
        #endregion

        #region Check Array
        /// <summary>
        /// Checks for an empy spot in the array.
        /// <para>TODO: We are gona put this in a custom array class. 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private bool CheckArray(out int index){

            for (int i = 0; i < this.MyNodes.Length; i++)
            {
                if(this.MyNodes[i] == null){
                                     
                    index = i;
                    return true;
                }
            }
            index = -1;
            return false;
        }
        #endregion

        #region Subvidide funcs
        /// <summary>
        /// Subdivides a <see cref="QuadTree<T>" in four zones
        /// </summary>
        /// <param name="quad">The <see cref="QuadTree<T>" to divide</param>
        private void Subdivide(QuadTree<T> quad){
            Vector2 tempVector = new Vector2((quad.MyShape.BottomRight.x - quad.MyShape.TopLeft.x )/2, 
                                             (quad.MyShape.BottomRight.y - quad.MyShape.TopLeft.y)/2);  

            float middleX = quad.MyShape.TopLeft.x + tempVector.x;
            
            // OJU!!!! Downing the Y it's positive!            
            float middleY = quad.MyShape.TopLeft.y + tempVector.y;

            //GD.Print("This quad: TL:" + this.TopLeft + "//BR:" + this.BottomRight);

            quad.TopLeftTree = new QuadTree<T>(quad.MyShape.TopLeft, new Vector2(middleX,middleY));  

            quad.BottomLeftTree = new QuadTree<T>(new Vector2(quad.MyShape.TopLeft.x, middleY), 
                                                  new Vector2(middleX, quad.MyShape.BottomRight.y));                                             
            
            quad.TopRigthTree = new QuadTree<T>(new Vector2(middleX, quad.MyShape.TopLeft.y), 
                                            new Vector2(quad.MyShape.BottomRight.x, middleY));

            quad.BottomRigthTree = new QuadTree<T>(new Vector2(middleX,middleY), quad.MyShape.BottomRight);
            
            //Tenemos que meter los puntos en este lado :S
            this.ArrayToNewNodes();          
        }        

        /// <summary>
        /// Passes the data of this <see cref="MyNodes"/> to the new build Quadtrees.
        ///<para>Used by default, is utilized to have only the data to the leaves.  
        /// </summary>
        private void ArrayToNewNodes(){
            QNode<T> tempNode;

            for (byte i = 0; i < this.Capacity; i++)
            {   
                if(this.MyNodes[i].HasValue){
                    tempNode = (QNode<T>) this.MyNodes[i];
                }
                else{
                    //TODO:
                    //We will make a class with an ordered array so, if the Array[0] is empty, means
                    //that the othear are empty aswell.
                    //(A list is better?)
                    continue;
                }

                if(tempNode.BoxShape != null){
                    this.Insert(tempNode.BoxShape, tempNode.MyData);
                    this.MyNodes[i] = null;
                }
                else{
                    this.Insert(tempNode.Position, tempNode.MyData);
                    this.MyNodes[i] = null;
                }                                
            }
        }
        #endregion

        #region Indexes
        /// <summary>
        /// Returns a child Quadtree based on an index.
        /// <para>
        /// 0 = TopLeftTree<para>
        /// 1 = TopRightTree<para>
        /// 2 = BottomRightTree<para>
        /// 3 = BottomLeftTree
        /// default = null;
        /// </summary>
        public QuadTree<T> this[int index]{
            get{ return SetIndexes(index);}
        }

        /// <summary>
        /// Return a Quatree child based on an index
        /// </summary>
        /// <param name="index">Index of the quadTree desired</param>
        /// <returns></returns>
        private QuadTree<T> SetIndexes(in int index){
            switch(index){
                case 0:
                    return this.TopLeftTree;                    
                case 1:
                    return this.TopRigthTree;
                case 2:
                    return this.BottomRigthTree;
                case 3:
                    return this.BottomLeftTree;
                default:
                    return null;
            }
        }
        #endregion
     
    }
}