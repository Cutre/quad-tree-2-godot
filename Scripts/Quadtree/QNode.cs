using Godot;
using System;

using MyPhysics.Colliders;

namespace Quadtree{
    /// <summary>
    /// The node struct that we put at the Quadtree
    /// </summary>
    public struct QNode<T>
    {
        #region Properties
        /// <summary>
        /// Data class. We can put wathever
        /// </summary>    
        public T MyData{get;set;}
        
        /// <summary>
        /// Position of the node.
        /// </summary>
        public Vector2 Position{get;set;}

        /// <summary>
        /// A reference to the object bounding box. Needed if the object it's inserted
        /// by its bounding box shape.
        /// </summary>
        /// <value></value>
        public QuadShape BoxShape{get;set;}
        #endregion
        public QNode(T myObject, Vector2 pos, QuadShape quad){
            this.MyData = myObject;
            this.Position = pos;
            this.BoxShape = quad;
        }            

    }
}