using Godot;
using System;
using System.Collections.Generic;

namespace Quadtree
{   
    public class QuadController : Node2D
    {
        QuadTree<Vector2> t;

        [Export]
        Color sideColor;

        [Export]
        Color pointColor;

        [Export]
        int numberPoint;

        [Export]
        int width;
        [Export]
        int heigh;

        
        enum Sort : byte{AUTOMATIC, MANUAL}
        [Export]
        Sort MySort;

        int index;
        
        public override void _Ready()
        {
            this.Init();        
        }

        private void Init(){
             t = new QuadTree<Vector2>(new Vector2(0,0), new Vector2(this.width,this.heigh));
            
            
            if(this.MySort == Sort.AUTOMATIC){
                this.AddPoints();  
            }           
            
            this.index = 0;   

        }


        public override void _Draw(){
            GD.Print("Draw");
            
            //this.Createquad(t.TopLeft, t.BottomRight, this.sideColor);
            this.DrawQuad();            
        }


        private void Createquad(in Vector2 topleft, in Vector2 bottomRight, in Color col, in float thick = 2){
            base.DrawLine(topleft, new Vector2(bottomRight.x, topleft.y), col, thick);
            base.DrawLine(new Vector2(bottomRight.x, topleft.y), bottomRight, col, thick);
            base.DrawLine(bottomRight, new Vector2(topleft.x, bottomRight.y), col, thick);
            base.DrawLine(new Vector2(topleft.x, bottomRight.y), topleft, col, thick);
        }
        
        private void AddPoints(){
            int randomX;
            int randomY;

            Random rand = new Random();

            bool isInserted = false;
            for(int i = 0; i < numberPoint; i++){
                randomX = rand.Next(0, this.width);
                randomY = rand.Next(0, this.heigh);

                Vector2 pos = new Vector2(randomX, randomY);                             
                //GD.Print("Pos: " + pos);
                isInserted = t.Insert(in pos, pos);
                //GD.Print("Node " + i + " inserted: " + isInserted);                           
            }
        }
        
        #region Draw
        private void DrawQuad(){
            this.DrawQuadTree(this.t);
        }

        
        private void DrawQuadTree(in QuadTree<Vector2> quad){

            this.Createquad(quad.MyShape.TopLeft, quad.MyShape.BottomRight, this.sideColor);

            QuadTree<Vector2> temp;

            for(byte i = 0; i < quad.Children; i++){
                temp = quad[i];

                if(temp == null){
                    this.DrawPoints(quad);
                    return;
                }

                //colocamos el nuevo como busqueda
                this.DrawQuadTree(temp);
            }
        }
        

        private void DrawPoints(in QuadTree<Vector2> quad){
            QNode<Vector2> tempNode;

            for(byte i = 0; i < quad.MyNodes.Length; i++){
                
                if(quad.MyNodes[i].HasValue){
                    tempNode = (QNode<Vector2>) quad.MyNodes[i];
                    base.DrawCircle(tempNode.Position, 4, this.pointColor);
                }
                
            }
        }
        #endregion
        
        private void AddOnlyOnePoint(){

            if(this.numberPoint <= this.index){
                return;
            }

            index++;

            int randomX;
            int randomY;

            Random rand = new Random();

            randomX = rand.Next(0, this.width);
            randomY = rand.Next(0, this.heigh);

            Vector2 pos = new Vector2(randomX, randomY);                             
            GD.Print("Pos: " + pos);
            bool isInserted = t.Insert(in pos, pos);
            GD.Print("Node " + index + " inserted: " + isInserted);
            base.Update();    

        }
        

        public override void _Process(float delta){
            
            if(Input.IsActionJustPressed("ui_select")){
                
                if(this.MySort == Sort.MANUAL){
                    this.AddOnlyOnePoint();
                    GD.Print("Pressed");
                }
                else{
                    this.Init();
                    base.Update();
                }
                
            }
            
           
        }


    }
}