using Godot;
using System;
using System.Collections.Generic;

using MyPhysics.Colliders;

namespace Quadtree.Controller
{
    /// <summary>
    /// Temp and not optimized class to create and hold the objecst
    /// </summary>
    public class QuadController2 : Node2D
    {           

        [Export]
        int capacity;
        [Export]
        public int Width;
        [Export]
        public int Height; 
        [Export]
        Color pointColor;
        [Export]
        Color sideColor;
        [Export]
        bool showBoxes;

        public QuadTree<Entity> QuadTree{get;set;}   

        public override void _Ready()
        {
            GD.Print("hola?");
            this.Init();  
            base.Update();
        }      

        private void Init(){
            if(this.QuadTree == null){
                this.QuadTree = CreateTree();
            }           

        }

        #region Draw
        public override void _Draw(){
            if(this.showBoxes){
                this.DrawQuad();
            }
            
        }

        private void DrawQuad(){
            if(this.QuadTree == null){
                return;
            }
            DrawQuadTree(this.QuadTree);
        }

         private void Createquad(in Vector2 topleft, in Vector2 bottomRight, in Color col, in float thick = 2){
            base.DrawLine(topleft, new Vector2(bottomRight.x, topleft.y), col, thick);
            base.DrawLine(new Vector2(bottomRight.x, topleft.y), bottomRight, col, thick);
            base.DrawLine(bottomRight, new Vector2(topleft.x, bottomRight.y), col, thick);
            base.DrawLine(new Vector2(topleft.x, bottomRight.y), topleft, col, thick);
        }
        #endregion

        public QuadTree<Entity> CreateTree(){
            Vector2 or = new Vector2(0,0);
            Vector2 bottRight = new Vector2(this.Width, this.Height);
            this.QuadTree = new QuadTree<Entity>(or, bottRight);
            GD.Print("Original tree: " + this.QuadTree);
            return this.QuadTree;
        }

        private void DrawQuadTree(in QuadTree<Entity> quad){
             this.Createquad(quad.MyShape.TopLeft, quad.MyShape.BottomRight, this.sideColor, 0.5f);

            QuadTree<Entity> temp;       

            for(byte i = 0; i < quad.Children; i++){
                temp = quad[i];

                if(temp == null){
                    this.DrawPoints(quad);
                    return;
                }

                //colocamos el nuevo como busqueda
                this.DrawQuadTree(temp);
            }
        }

        private void DrawPoints(in QuadTree<Entity> quad){
            QNode<Entity> tempNode;

            for(byte i = 0; i < quad.MyNodes.Length; i++){
                
                if(quad.MyNodes[i].HasValue){
                    tempNode = (QNode<Entity>) quad.MyNodes[i];
                    base.DrawCircle(tempNode.Position, 0.5f, this.pointColor);
                }
                
            }
        }

       
      


    }
}