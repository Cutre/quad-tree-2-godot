using Godot;
using System;

using MyPhysics.Collision.AABB;

namespace MyPhysics.Colliders
{ 
    public class Body 
    {
        public Vector2 VelocityVector{get;set;}
        public Vector2 Position{get;set;}        
        public float Speed{get;set;}

        public Shape MyShape{get;set;}

        
       

    }
}