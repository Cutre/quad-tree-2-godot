using Godot;
using System;

using Quadtree;
using Quadtree.Controller;
using System.Collections.Generic;

namespace MyPhysics.Colliders
{   
    /// <summary>
    /// Temp and not optimized class to make test 
    /// </summary>
    public class PlayerObject : Node2D
    {
        Entity testEnt;      
        
        [Export]
        Color nor;
        
        [Export]
        Color coll;
        [Export]
        float speed;

        Vector2 movVector;

        QuadTree<Entity> root;
        public override void _Ready(){
            this.Init();
            QuadController2 q = (QuadController2)base.GetNode(@"/root/Node/QuadController");
            this.root = q.QuadTree;
        }

        private void Init(){
            Vector2 pos = new Vector2(20,20);
            int dimX = 10;
            int dimY = 10;
            Vector2 topLeft = new Vector2(pos.x, pos.y);
            Vector2 bottright = new Vector2(pos.x + dimX, pos.y + dimY);
            QuadShape quad = new QuadShape(topLeft, bottright);

            this.testEnt = new Entity(quad, quad, pos, nor, coll);
        }

        public override void _Draw(){
            this.testEnt.Draw(this, false, true);
        }

        public override void _Process(float delta){
            this.GetInput();
            if(this.movVector != Vector2.Zero){
                this.MoveObject(delta);
            }
            
        }

        private void GetInput(){
            this.movVector = Vector2.Zero;

            if(Input.IsKeyPressed((int)KeyList.Up)){
                this.movVector.y = -1;
            }
            else if(Input.IsKeyPressed((int)KeyList.Down)){
                this.movVector.y = 1;
            }

            if(Input.IsKeyPressed((int)KeyList.Left)){
                this.movVector.x = -1;
            }
            else if(Input.IsKeyPressed((int)KeyList.Right)){
                this.movVector.x = 1;
            }
        }

        private void MoveObject(in float delta){
            Vector2 pos = this.testEnt.Position;
            pos += (movVector * this.speed * delta);
            this.testEnt.ChangePos(pos);
            Search();
            //Check coll
            base.Update();
        }

        private void Search(){
            List<Entity> temp = this.root.GetObjects(this.testEnt.BoundingBox);
            //GD.Print(temp.Count);
            if(temp == null || temp.Count == 0){
                this.testEnt.Collided = false;
                return;
            }

            this.CheckListAndCollide(temp);

        }

        private void CheckListAndCollide(in List<Entity> ents){
            for(int i = 0; i < ents.Count; i++){
                if(ents[i].BoundingBox.Intersects(this.testEnt.BoundingBox)){
                    this.testEnt.Collided = true;
                    //GD.Print("Ey Collision!");
                    return;
                }
                this.testEnt.Collided = false;
            }
        }
    
    }

}