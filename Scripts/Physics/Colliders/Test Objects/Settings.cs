using Godot;
using System;

public class Settings : Node
{
   public override void _Ready(){
       Vector2 newSize = OS.GetWindowSize() * 3;
       Vector2 windowPos = OS.GetScreenSize()/2 - newSize/2;

       OS.SetWindowSize(newSize);
       OS.SetWindowPosition(windowPos);
       GD.Print("Size!!!");
   }

   public override void _Process(float delta){
       if(Input.IsKeyPressed((int)KeyList.Escape)){
           GetTree().Quit();
       }
       if(Input.IsKeyPressed((int)KeyList.F11)){
           OS.SetWindowFullscreen(!OS.WindowFullscreen);
       }
   }
}
