using Godot;
using System;
using System.Collections.Generic;

using Quadtree;
using Quadtree.Controller;

namespace MyPhysics.Colliders
{    
    /// <summary>
    /// Temp and not optimized class to create and hold the objecst
    /// </summary>
    public class ObjectController : Node2D
    {
        QuadTree<Entity> root;
        List<Entity> myObjects;

        [Export]
        int capacity;
        [Export]
        Color norCol;

        [Export]
        Color collColor;

        int width;
        int height;

        #region InitFunc
        public override void _Ready(){
            this.Init();
        }

        private void Init(){
            
            this.myObjects = new List<Entity>(this.capacity);
            GetMyParent();
            PopulateObjects();
            InsertAllToQuadTree();
            
        }

        private void GetMyParent(){
            QuadController2 tempCon = (QuadController2) base.GetParent();
            this.root = tempCon.CreateTree();
            this.width = tempCon.Width;
            this.height = tempCon.Height;

            GD.Print("Obj Controller tree: " + this.root);
            GD.Print("Obj Controller width: " + this.width);
            GD.Print("Obj Controller height: " + this.height);
        }

        private void InsertToQuadTree(Entity ent){
            /*Vector2[] points = ent.GetBoundingBoxPoints();
            for(int i = 0; i < 4; i++){
                this.root.Insert(points[i], ent);
            } */
            this.root.Insert(ent.BoundingBox, ent);           
        }

        private void InsertAllToQuadTree(){
            for(int i = 0; i < this.myObjects.Count; i++){
                this.InsertToQuadTree(this.myObjects[i]);
            }
        }
       
        #endregion

        #region Populate
        private void PopulateObjects(){
            Random rand = new Random();

            int x;
            int y;

            int maxW = 12;
            int maxH = 12;
            QuadShape tempQuad;
            Entity ent = null;
            bool collision = true;

            for(int i = 0; i < this.capacity; i++){
                //Primero creamos cuadrados :D
                collision = true;
                //GD.Print("i" + i);
                while(collision){
                    x = rand.Next(2,this.width - maxW);
                    y = rand.Next(2, this.height - maxH);

                    Vector2 topLeft = new Vector2(x, y);

                    x = rand.Next( 1, maxW);
                    y = rand.Next( 1, maxH);

                    Vector2 bottR = new Vector2(topLeft.x + x,topLeft.y + y);

                    tempQuad = new QuadShape(topLeft, bottR);
                    ent = new Entity(tempQuad, tempQuad, topLeft, this.norCol, this.collColor);                    
                    //check collision
                    collision = Collision(ent);                                        
                }
                //GD.Print("Ent TL: " + ent.BoundingBox.TopLeft + " BR: " + ent.BoundingBox.BottomRight);
                this.myObjects.Add(ent);               
                
            }
        }
        #endregion

        private bool Collision(in Entity ent){
            for(int j = 0; j < this.myObjects.Count; j++){
                if(ent.Shape.Intersects(this.myObjects[j].Shape)){
                    //GD.Print("Collision");
                    return true;
                }
            }   
            return false;
        }

        #region Draw

        public override void _Draw(){
            PaintObjects();

        }

        private void PaintObjects(){
            for(int i = 0; i < this.myObjects.Count; i++){
                this.myObjects[i].Draw(this, false, true);
            }
        }
        #endregion
    
    }
    
}