using Godot;
using System;

namespace MyPhysics.Colliders
{
    public class MovEntity : Entity
    {
        public MovEntity(in MyShape shape, in QuadShape boundingBox, in Vector2 pos, in Color normalCol, in Color collColl) : base(shape, boundingBox, pos, normalCol, collColl)
        {
        }
    }
}