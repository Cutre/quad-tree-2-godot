using Godot;
using System;

namespace MyPhysics.Colliders
{ 
    public class Entity 
    {
        public MyShape Shape{get;set;}
        public QuadShape BoundingBox{get;set;}

        public Vector2 Position{get;set;}

        public bool Collided;
        private Color normalCol;
        private Color collCol;
        public Entity(in MyShape shape, in QuadShape boundingBox, in Vector2 pos, in Color normalCol, in Color collColl){
            this.Shape = shape;
            this.BoundingBox = boundingBox;
            this.Position = pos;
            this.normalCol = normalCol;
            this.collCol = collColl;
        }

        public void Draw(in CanvasItem item, in bool boundingBox, in bool obj){
            

            if(boundingBox){
                this.BoundingBox.Draw(item, this.normalCol, false);
            }
            if(obj){
                Color temp;
                if(this.Collided){
                    temp = collCol;
                }
                else{
                    temp = normalCol;
                }
                this.Shape.Draw(item, temp, true);
            }
        }

        public Vector2[] GetBoundingBoxPoints(){
            Vector2[] points = new Vector2[4];
            for(int i = 0; i < 4; i++){
                points[i] = this.BoundingBox[i];
            }            
            return points;
        }

        //TODO: This, another script
        public void ChangePos(in Vector2 newPos){
            this.Position = newPos;
            float width = this.BoundingBox.Width;
            float height = this.BoundingBox.Height;

            this.BoundingBox.TopLeft = new Vector2(newPos.x, newPos.y);
            this.BoundingBox.BottomRight = new Vector2(newPos.x + width, newPos.y + height);                        
        }
      
    
    }
}