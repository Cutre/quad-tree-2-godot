using Godot;
using System;

using MyPhysics.Collision.AABB;

namespace MyPhysics.Colliders{

    public class QuadShape : MyShape
    {
        #region Properties
        /// <summary>
        /// The Top Left corner of the Quad
        /// </summary>
        /// <value></value>
        public Vector2 TopLeft{get;set;}

        /// <summary>
        /// The Bottom Right corner of the Quad
        /// </summary>
        /// <value></value>
        public Vector2 BottomRight{get;set;}

        /// <summary>
        /// The Quads Width
        /// </summary>
        /// <value></value>
        public float Width{
            get{return this.BottomRight.x - this.TopLeft.x;}
        }

        /// <summary>
        /// The Quads height
        /// <para>OJU! Getting Down augments the Y value. Inverse if the engine decreases Y when getting down!!!
        /// </summary>
        /// <value></value>
        public float Height{
            get{return this.BottomRight.y - this.TopLeft.y;}
        }

        /// <summary>
        /// The Top Right corner.
        /// <para>Calculated value.
        /// </summary>
        /// <value></value>
        public Vector2 TopRight{
            get{return new Vector2(this.BottomRight.x, this.TopLeft.y);}
        }

        /// <summary>
        /// The bottom Left corner.
        /// <para>Calculated value
        /// </summary>
        /// <value></value>
        public Vector2 BottomLeft{
            get{return new Vector2(this.TopLeft.x, this.BottomRight.y);}
        }
        
        #endregion 
       
        #region Constructors
        public QuadShape(in Vector2 topLeft, in Vector2 bottomRight){
            this.TopLeft = topLeft;
            this.BottomRight = bottomRight;                 
        }

        public QuadShape(in float tlx, in float tly, in float brx, in float bry){
            this.TopLeft = new Vector2(tlx, tly);
            this.BottomRight = new Vector2(brx, bry);            
        }
        #endregion

        #region Intersects        
        public override bool Intersects(in MyShape other){
            return other.Intersects(this);            
        }

        public override bool Intersects(in QuadShape quad){            
            return CollisionManager.QuadVSQuadNoContact(this, quad);            
        }

        public override bool Intersects(in CircleShape circle){
            return CollisionManager.QuadVSCircle(this, circle);
        }

        public override bool Intersects(in Vector2 point){
            return CollisionManager.QuadVSPoint(this, point);
        }
        #endregion
       
        #region Indexer
        public Vector2 this[in int index]{
            get{return this.Corner(index);}
        }

        /// <summary>
        /// Gets a corner by an index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private Vector2 Corner(in int index){
            switch(index){
                case 0:
                    return this.TopLeft;
                case 1:
                    return this.TopRight;
                case 2:
                    return this.BottomLeft;
                case 3:
                    return this.BottomRight;
                default:
                    return new Vector2(-50000, -50000);
            }
        }   
        #endregion

        #region Draw
        
        public override void Draw(in CanvasItem canv, in Color nor, in bool fill)
        {
            canv.DrawRect(new Rect2(this.TopLeft, 
                          new Vector2(this.Width, this.Height)),nor, fill);

        }
        #endregion
    }
}