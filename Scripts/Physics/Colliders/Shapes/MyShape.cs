using Godot;
using System;

namespace MyPhysics.Colliders
{
    /// <summary>
    /// Abstract class to group all the shapes.
    /// <para>OJU! There's not point class at the moment
    /// </summary>
    public abstract class MyShape 
    {
        public abstract bool Intersects(in MyShape other);

        public abstract bool Intersects(in QuadShape shape);  
        public abstract bool Intersects(in CircleShape circle); 
        public abstract bool Intersects(in Vector2 point);

        /// <summary>
        /// OJU!!!! Only to test!
        /// </summary>
        /// <param name="item"></param>
        public abstract void Draw(in CanvasItem item, in Color nor, in bool fill);     
    }
}