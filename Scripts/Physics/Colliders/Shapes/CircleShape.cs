using Godot;
using System;

using MyPhysics.Collision.AABB;

namespace MyPhysics.Colliders
{
    public class CircleShape : MyShape
    {
        public float Radius{get;set;}  

        public Vector2 Center{get;set;}      

        public CircleShape(in Vector2 center, in float radius){
            this.Radius = radius;      
            this.Center = center;      
        }

        #region Intersects
        public override bool Intersects(in MyShape other){
            return other.Intersects(this);           
        }

        public override bool Intersects(in QuadShape quad){
            return CollisionManager.QuadVSCircle(quad, this);            
        }

        public override bool Intersects(in CircleShape circle){
            return CollisionManager.CircleVSCircle(this, circle);            
        }

        public override bool Intersects(in Vector2 point){
            return false;
        }
        #endregion

        public override void Draw(in CanvasItem item, in Color nor, in bool fill)
        {
            item.DrawCircle(this.Center, this.Radius, nor);
        }
        
    }
}