using Godot;
using System;

using MyPhysics.Colliders;

namespace MyPhysics.Collision.AABB
{
    /// <summary>
    /// Manages the Objects collision detection
    /// </summary>
    public static class CollisionManager
    {
        /// <summary>
        /// Check collision between two <see cref="QuadShape"/> with contact considered collision
        /// </summary>
        /// <param name="a">QuadShape A</param>
        /// <param name="b">QuadShape B</param>
        /// <returns>Collided?</returns>
        public static bool QuadVSQuadContact(in QuadShape a, in QuadShape b){
            return!(a.TopLeft.x >= b.BottomRight.x ||
                  a.TopLeft.x <= b.BottomRight.x ||
                  a.BottomLeft.y <= b.TopLeft.y ||
                  a.TopLeft.y >= b.BottomRight.y);            
        }

        /// <summary>
        /// Checks collision between two <see cref="QuadShape"/> with contact not considered collision
        /// </summary>
        /// <param name="a">QuadShape A</param>
        /// <param name="b">QuadShape B</param>
        /// <returns>Collided?</returns>
        public static bool QuadVSQuadNoContact(in QuadShape a, in QuadShape b){
            return!(a.BottomRight.x < b.TopLeft.x ||
                    a.TopLeft.x > b.BottomRight.x ||
                    a.BottomRight.y < b.TopLeft.y ||
                    a.TopLeft.y > b.BottomRight.y);
        }

        /// <summary>
        /// Checks collision between a <see cref="QuadShape"/> and a <see cref="CircleShape"/>
        /// </summary>
        /// <param name="quad">QuadShape</param>
        /// <param name="circle">CircleShape</param>
        /// <returns>Collided?</returns>
        public static bool QuadVSCircle(in QuadShape quad, in CircleShape circle){
            float circleDistX = Mathf.Abs(circle.Center.x - (quad.TopLeft.x + quad.Width/2));
            float circleDistY = Mathf.Abs(circle.Center.y - (quad.TopLeft.y + quad.Height/2));          

            throw new NotImplementedException("Quad Vs circle not completed!");
            
        }

        /// <summary>
        /// Checks collision between a <see cref="QuadShape"/> and a point
        /// </summary>
        /// <param name="quad">QuadShape</param>
        /// <param name="point">Vector2 point</param>
        /// <returns>Collided?</returns>
        public static bool QuadVSPoint(in QuadShape quad, in Vector2 point){
            return(quad.TopLeft.x <= point.x &&
                   quad.TopLeft.y <= point.y &&
                   quad.BottomRight.x >= point.x &&
                   quad.BottomRight.y >= point.y);
        }

        /// <summary>
        /// Checks Collision between two <see cref="CircleShape"/>
        /// </summary>
        /// <param name="a">CircleShape A</param>
        /// <param name="b">CircleShape B</param>
        /// <returns>Collided?</returns>
        public static bool CircleVSCircle(in CircleShape a, in CircleShape b){
            float sqRadSum = (a.Radius + b.Radius) * (a.Radius + b.Radius);
            float distance = (b.Center.x - a.Center.x) * (b.Center.x - a.Center.x) + 
                             (b.Center.y - a.Center.y) * (b.Center.y - a.Center.y);           
            return sqRadSum > distance;
        }



        

       

        

       


    }
}